### Building and starting the application ###

To build the backend and start the server, navigate to  the backend directory and run the following command:

   node server.js

After the server starts it will be accessible on port 5000

To start frontend, navigate to  the frontend directory and run the following command:

   npm start

After the frontend starts it will be accessible on port 3000.