const express = require("express");
const router = express.Router();
const moment = require("moment");
const Database  = require('../config/database');
const database = new Database();
const request = require("request");
const mysql = require('mysql');
const KNN = require('ml-knn');



router.post("/predict/hepatitis", (req, res_, next) => {
	const { dataArr } = req.body;
	const sql = `SELECT * FROM records`;
	const errorCounter = (predicted, expected) => {
		let misclassifications = 0;
		for (var index = 0; index < predicted.length; index++) {
			if (predicted[index] !== expected[index]) {
				misclassifications++;
			}
		}
		return misclassifications;
	};

	const shuffle = arr => {
		for (var i = arr.length - 1; i > 0; i--) {
			var j = Math.floor(Math.random() * (i + 1));
			var temp = arr[i];
			arr[i] = arr[j];
			arr[j] = temp;
		}
		return arr;
	};
	database
		.query(sql)
		.then(res => {
			let seperationSize;
			let data = [],
				X = [],
				y = [];
			data = shuffle(res);
			seperationSize = 0.7 * data.length;

			let trainingSetX = [],
				trainingSetY = [],
				testSetX = [],
				testSetY = [];
			let types = new Set();
			data.forEach(row => {
				types.add(row.class);
			});
			typesArray = [...types];
			data.forEach(row => {
				let rowArray, typeNumber;
				let type = row.class;
				delete row.class;
				delete row.id;
				row.sex = row.sex == "male" ? 1 : 0;
				rowArray = Object.keys(row).map(key => parseFloat(row[key]));

				typeNumber = typesArray.indexOf(type);

				X.push(rowArray);
				y.push(typeNumber);
			});
			console.log(typesArray);

			trainingSetX = X.slice(0, seperationSize);
			trainingSetY = y.slice(0, seperationSize);
			testSetX = X.slice(seperationSize);
			testSetY = y.slice(seperationSize);
			knn = new KNN(trainingSetX, trainingSetY, { k: 7 });
			const resultTest = knn.predict(testSetX);
			const testSetLength = testSetX.length;
			console.log(testSetLength);
			const predictionError = errorCounter(resultTest, testSetY);
			console.log(predictionError);
            //let temp = [51, null, null, 1, 0, 0, null, 1, 0, 1, 0, 0, null, 0, null, 1, 1, 0, 0]
            console.log(dataArr);
			res_.status(200).json({ class: typesArray[knn.predict(dataArr.map(Number))] });
		})
		.catch(err => {
			console.log(err);
			res_.status(500).json({
				data: "Internal Error"
			});
		});
});


router.get('/initializeData', (req,res,next) => {
    const url = 'https://pkgstore.datahub.io/machine-learning/hepatitis/hepatitis_json/data/06ce7029f0677bc770ad52e9b273bbfd/hepatitis_json.json';
    request({
        url,
        json: true
    }, function (error, response, body) {

        if (!error && response.statusCode === 200) {
            let sql = `INSERT INTO records(${Object.keys(body[0]).join(',')}) VALUES`;
            body.forEach(element => {
            sql += `(${Object.keys(element).reduce((values, key, ind) => {
                return values + mysql.escape(element[key]) + (ind == Object.keys(element).length - 1  ? "" : ",")
            },"")}),`
            });
            sql = sql.slice(0, -1);
            database.query(sql)
            .then((result) => {
                console.log(result);
            }).catch((err) => {
                console.log(err);    
            });
        }
    });
})

router.get("/getByAttribute/:attribute", (req, res, next) => {
	const { attribute } = req.params;
	const sql =
		attribute == "all"
			? `SELECT * FROM records`
			: `SELECT ${attribute} FROM records`;
	database
		.query(sql)
		.then(result => {
			res.status(200).json(result);
		})
		.catch(err => {
			res.status(500).json({
				data: "Internal Error"
			});
		});
});

router.post("/create", (req, res, next) => {
	const columnsNames = Object.keys(req.body)
		.reduce((columnsNames, column) => columnsNames + column + ",", "")
		.slice(0, -1);
	const values = Object.keys(req.body)
		.reduce(
			(values, column) => values + mysql.escape(req.body[column]) + ",",
			""
		)
		.slice(0, -1);
    const sql = `INSERT INTO records (${columnsNames}) VALUES (${values})`;
    database.query(sql)
    .then((result) => {
        res.status(200).json({
            "data": "success"
        })
    }).catch((err) => {
        console.log(err);    
    });
    
});


module.exports = router;
