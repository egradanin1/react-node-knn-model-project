import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Table from "components/Table/Table.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import axios from "axios";

const styles = {
	cardCategoryWhite: {
		"&,& a,& a:hover,& a:focus": {
			color: "rgba(255,255,255,.62)",
			margin: "0",
			fontSize: "14px",
			marginTop: "0",
			marginBottom: "0"
		},
		"& a,& a:hover,& a:focus": {
			color: "#FFFFFF"
		}
	},
	cardTitleWhite: {
		color: "#FFFFFF",
		marginTop: "0px",
		minHeight: "auto",
		fontWeight: "300",
		fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
		marginBottom: "3px",
		textDecoration: "none",
		"& small": {
			color: "#777",
			fontSize: "65%",
			fontWeight: "400",
			lineHeight: "1"
		}
	}
};

class TableList extends React.Component{
	state = {
		records: []
	}
	componentDidMount() {
		axios.get("/api/getByAttribute/all")
		.then((result) => {
			let records = result.data.map(record => Object.keys(record).map(key => String(record[key])));
			this.setState({records});
			console.log(records);
		}).catch((err) => {
			console.log(err);
		});
	}

	render() {
		const { classes } = this.props;
		const { records } = this.state;
		return (
			<GridContainer>
				<GridItem xs={12} sm={12} md={12}>
					<Card>
						<CardHeader color="primary">
							<h4 className={classes.cardTitleWhite}>
								Hepatis Data
							</h4>
							<p className={classes.cardCategoryWhite}>Records</p>
						</CardHeader>
						<CardBody>
							<Table
								tableHeaderColor="primary"
								tableHead={[
									"Id",
									"Age",
									"Albumin",
									"Alk Phosphate",
									"Anorexia",
									"Antivirals",
									"Ascites",
									"Bilirubin",
									"Class",
									"Fatigue",
									"Histology",
									"Liver big",
									"Liver firm",
									"Malaise",
									"Protime",
									"Sex",
									"Sgot",
									"Spiders",
									"Spleen palpable",
									"Steroid",
									"Varices"
								]}
								tableData={records}
							/>
						</CardBody>
					</Card>
				</GridItem>
			</GridContainer>
		);
	}
}

export default withStyles(styles)(TableList);
