import React from "react";
import PropTypes from "prop-types";

import withStyles from "@material-ui/core/styles/withStyles";

import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";


import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";


import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";

import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";


import {
	BarChart,
	Bar,
	Cell,
	XAxis,
	YAxis,
	CartesianGrid,
	Tooltip,
	Legend,
	ComposedChart,
	Line,
	Area,
	PieChart,
    Pie,
    Sector
} from "recharts";

import axios from "axios";

let lastUpdateTime = null;

class Analysis extends React.Component {
	state = {
		value: 0,
        chartData: [],
        selectedCheckboxes: new Set(),
        gender: "male",
        inputValues: {},
        liveDieData: [{name: "live", total: 0},{name: "die", total: 0}]
    };

	componentDidMount() {
		this.fetchData();
    }
    
    toggleCheckbox = label => {
        let { selectedCheckboxes } = this.state;
        if (selectedCheckboxes.has(label)) {
          selectedCheckboxes.delete(label);
        } else {
          selectedCheckboxes.add(label);
        }
        lastUpdateTime = Date.now();
        this.setState({selectedCheckboxes}, this.getLiveDiedChart)
    }

    handleInputChange = (e, type) => {
        let { inputValues } = this.state;
        lastUpdateTime = Date.now();
        this.setState({inputValues: {...inputValues, [type]: e.target.value}}, this.getLiveDiedChart) 
    }

    setGender(event) {
        lastUpdateTime = Date.now();
        this.setState({gender: event.target.value}, this.getLiveDiedChart);
    }

    getLiveDiedChart = () => {
        let { selectedCheckboxes, gender, inputValues, chartData } = this.state;
       
            // gender
            // checkbox
            // inputValues
            let liveDieData = [{name: "live", total: 0},{name: "die", total: 0}]
            chartData
                .filter(row => row.sex == gender || gender == "all")
                .filter(row => 
                    Array.from(selectedCheckboxes)
                        .every(label => row[label] == 1)
                )
                .filter(row => 
                    Object.keys(inputValues).every(constraint => {
                        let type = constraint.substring(0,3);
                        let prop = constraint.substring(4);
                        let test = true;
                        if (type == "max"){
                            test = row[prop] <= inputValues[constraint];
                        }else if (type == "min"){
                            test = row[prop] >= inputValues[constraint];
                        }
                        console.log(test);
                        return test
                    })
                )
                .forEach(row => {
                    if (row.class == "live"){
                        liveDieData[0].total = liveDieData[0].total + 1;
                    }else if (row.class == "die"){
                        liveDieData[1].total = liveDieData[1].total + 1;
                    }
                })
            this.setState({liveDieData});

        
    }



    isChecked = label => 
        this.state.selectedCheckboxes.has(label)


    createCheckbox = labelText => (
        <label>
          <input
            type="checkbox"
            value={labelText}
            checked={this.isChecked(labelText)}
            onChange={() => this.toggleCheckbox(labelText)}
         />

          {labelText}
        </label>
    )

    
    

	fetchData() {
		axios
			.get(`/api/getByAttribute/all`)
			.then(result => {
				this.setState({ chartData: result.data });
			})
			.catch(err => {
				console.log(err);
			});
    }
    
	render() {
		const { classes } = this.props;
        const { chartData, checkBoxItems, value, liveDieData } = this.state;
        const items = ["antivirals", "ascites", "steroid", "varices",  "liver_big" , "liver_firm", "spiders", "spleen_palpable", "anorexia", "fatigue", "histology", "malaise" ];
        let afhm = [{name: "anorexia", da: 0, ne: 0}, {name: "fatigue", da: 0, ne: 0}, {name: "histology", da: 0, ne: 0}, {name: "malaise", da: 0, ne: 0}]
        let ageData = [{name: "1-16",total: 0},{name: "17-32",total: 0}, {name: "33-48",total: 0}, {name: "48-63",total: 0}, {name: "64-80",total: 0}]
        let sexData = [{name: "male", total: 0}, {name: "female", total: 0}];
        let aasv = [{name: "antivirals", da: 0, ne: 0}, {name: "ascites", da: 0, ne: 0}, {name: "steroid", da: 0, ne: 0}, {name: "varices", da: 0, ne: 0}]
        let llss = [{name: "liver_big", da: 0, ne: 0}, {name: "liver_firm", da: 0, ne: 0}, {name: "spiders", da: 0, ne: 0}, {name: "spleen_palpable", da: 0, ne: 0}]
        let alk_phosphateData = [{name: "0-75",total: 0},{name: "76-150",total: 0}, {name: "151-225",total: 0}, {name: "226-300",total: 0}]
        let bilirubinData = [{name: "0-3",total: 0},{name: "4-7",total: 0}, {name: "8-10",total: 0}]
        let protimeData = [{name: "0-30",total: 0},{name: "31-60",total: 0}, {name: "61-100",total: 0}]
        let sgotData = [{name: "0-100",total: 0},{name: "101-350",total: 0}, {name: "351-650",total: 0}]
        
        const COLORS = ['#0088FE', '#00C49F'];

        const RADIAN = Math.PI / 180;
        const renderCustomizedLabel = ({
            cx, cy, midAngle, innerRadius, outerRadius, percent, index,
        }) => {
            const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
            const x = cx + radius * Math.cos(-midAngle * RADIAN);
            const y = cy + radius * Math.sin(-midAngle * RADIAN);
            let labelText = index == 1 ? "Female" : "Male";
            return (
                <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
                    {`${(percent * 100).toFixed(0)}%`}
                </text>
            );
        };
        chartData.forEach(row => {
            if (row.anorexia == 1){
                afhm[0].da = afhm[0].da + 1;
            }else{
                afhm[0].ne = afhm[0].ne + 1;
            }
            if (row.fatigue == 1){
                afhm[1].da = afhm[0].da + 1;
            }else {
                afhm[1].ne = afhm[0].ne + 1;
            }
            if (row.histology == 1){
                afhm[2].da = afhm[2].da + 1;
            }else {
                afhm[2].ne = afhm[2].ne + 1;
            }
            if (row.malaise == 1){
                afhm[3].da = afhm[3].da + 1;
            }else {
                afhm[3].ne = afhm[3].ne + 1;
            }

            if (row.age >= 1 && row.age <= 16){
                ageData[0].total = ageData[0].total + 1;
            }else if (row.age >= 17 && row.age <= 32){
                ageData[1].total = ageData[1].total + 1;
            }else if (row.age >= 33 && row.age <= 48){
                ageData[2].total = ageData[2].total + 1;
            }else if (row.age >= 48 && row.age <= 63){
                ageData[3].total = ageData[3].total + 1;
            }else {
                ageData[4].total = ageData[4].total + 1;
            }

            if (row.sex == "male"){
                sexData[0].total = sexData[0].total + 1;
            }else {
                sexData[1].total = sexData[1].total + 1;
            }


            if (row.antivirals == 1){
                aasv[0].da = aasv[0].da + 1;
            }else{
                aasv[0].ne = aasv[0].ne + 1;
            }
            if (row.ascites == 1){
                aasv[1].da = aasv[0].da + 1;
            }else {
                aasv[1].ne = aasv[0].ne + 1;
            }
            if (row.steroid == 1){
                aasv[2].da = aasv[2].da + 1;
            }else {
                aasv[2].ne = aasv[2].ne + 1;
            }
            if (row.varices == 1){
                aasv[3].da = aasv[3].da + 1;
            }else {
                aasv[3].ne = aasv[3].ne + 1;
            }


            if (row.liver_big == 1){
                llss[0].da = llss[0].da + 1;
            }else{
                llss[0].ne = llss[0].ne + 1;
            }
            if (row.liver_firm == 1){
                llss[1].da = llss[0].da + 1;
            }else {
                llss[1].ne = llss[0].ne + 1;
            }
            if (row.spiders == 1){
                llss[2].da = llss[2].da + 1;
            }else {
                llss[2].ne = llss[2].ne + 1;
            }
            if (row.spleen_palpable == 1){
                llss[3].da = llss[3].da + 1;
            }else {
                llss[3].ne = llss[3].ne + 1;
            }


            if (row.alk_phosphate >= 0 && row.alk_phosphate <= 75){
                alk_phosphateData[0].total = alk_phosphateData[0].total + 1;
            }else if (row.alk_phosphate >= 76 && row.alk_phosphate <= 150){
                alk_phosphateData[1].total = alk_phosphateData[1].total + 1;
            }else if (row.alk_phosphate >= 151 && row.alk_phosphate <= 225){
                alk_phosphateData[2].total = alk_phosphateData[2].total + 1;
            }else {
                alk_phosphateData[3].total = alk_phosphateData[3].total + 1;
            }

            if (row.bilirubin >= 0 && row.bilirubin <= 3){
                bilirubinData[0].total = bilirubinData[0].total + 1;
            }else if (row.bilirubin >= 4 && row.bilirubin <= 7){
                bilirubinData[1].total = bilirubinData[1].total + 1;
            }else if (row.bilirubin >= 8 && row.bilirubin <= 10){
                bilirubinData[2].total = bilirubinData[2].total + 1;
            }

            if (row.protime >= 0 && row.protime <= 30){
                protimeData[0].total = protimeData[0].total + 1;
            }else if (row.protime >= 31 && row.protime <= 60){
                protimeData[1].total = protimeData[1].total + 1;
            }else if (row.protime >= 61 && row.protime <= 100){
                protimeData[2].total = protimeData[2].total + 1;
            }

            if (row.sgot >= 0 && row.sgot <= 100){
                sgotData[0].total = sgotData[0].total + 1;
            }else if (row.sgot >= 101 && row.sgot <= 350){
                sgotData[1].total = sgotData[1].total + 1;
            }else if (row.sgot >= 351 && row.sgot <= 600){
                sgotData[2].total = sgotData[2].total + 1;
            }
        })

        
		return (
			<div>
				<GridContainer>
					<GridItem xs={12} sm={12} md={12}>
						<Card>
							<CardHeader color="warning">
								<h4 className={classes.cardTitleWhite}>
                                    Age and Sex
								</h4>
							</CardHeader>
							<CardBody>
                                <ComposedChart
                                    width={500}
                                    height={400}
                                    data={ageData}
                                    margin={{
                                    top: 20, right: 20, bottom: 20, left: 20,
                                    }}
                                >
                                    <CartesianGrid stroke="#f5f5f5" />
                                    <XAxis dataKey="name" />
                                    <YAxis />
                                    <Tooltip />
                                    <Legend />
                                    <Bar dataKey="total" barSize={20} fill="#413ea0" />
                                    <Line type="monotone" dataKey="total" stroke="#ff7300" />
                                </ComposedChart>
                                <PieChart width={400} height={400}>
                                    <Pie
                                    data={sexData}
                                    cx={200}
                                    cy={200}
                                    labelLine={true}
                                    label={renderCustomizedLabel}
                                    outerRadius={80}
                                    fill="#8884d8"
                                    dataKey="total"
                                    >
                                    {
                                        sexData.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)
                                    }
                                    </Pie>
                                    <Tooltip />
                                </PieChart>
							</CardBody>
						</Card>
					</GridItem>
                    <GridItem xs={12} sm={12} md={12}>
						<Card>
							<CardHeader color="info">
								<h4 className={classes.cardTitleWhite}>
                                    Anorexia, Fatigue, Histology, Malaise
								</h4>
							</CardHeader>
							<CardBody>
                                <BarChart
                                    width={500}
                                    height={300}
                                    data={afhm}
                                    margin={{
                                    top: 20, right: 30, left: 20, bottom: 5,
                                    }}
                                >
                                    <CartesianGrid strokeDasharray="3 3" />
                                    <XAxis dataKey="name" />
                                    <YAxis />
                                    <Tooltip />
                                    <Legend />
                                    <Bar dataKey="da" stackId="a" fill="#8884d8" />
                                    <Bar dataKey="ne" stackId="a" fill="#82ca9d" />
                                </BarChart>
							</CardBody>
						</Card>
					</GridItem>
                    <GridItem xs={12} sm={12} md={12}>
						<Card>
							<CardHeader color="warning">
								<h4 className={classes.cardTitleWhite}>
                                    Antivirals, Ascites, Steroid, Varices
								</h4>
							</CardHeader>
							<CardBody>
                                <BarChart
                                    width={500}
                                    height={300}
                                    data={aasv}
                                    margin={{
                                    top: 20, right: 30, left: 20, bottom: 5,
                                    }}
                                >
                                    <CartesianGrid strokeDasharray="3 3" />
                                    <XAxis dataKey="name" />
                                    <YAxis />
                                    <Tooltip />
                                    <Legend />
                                    <Bar dataKey="da" stackId="a" fill="#8884d8" />
                                    <Bar dataKey="ne" stackId="a" fill="#82ca9d" />
                                </BarChart>
							</CardBody>
						</Card>
					</GridItem>
                    <GridItem xs={12} sm={12} md={12}>
						<Card>
							<CardHeader color="danger">
								<h4 className={classes.cardTitleWhite}>
                                    Big liver, Firm Liver, Spiders, Palpable Spleen
								</h4>
							</CardHeader>
							<CardBody>
                                <BarChart
                                    width={500}
                                    height={300}
                                    data={llss}
                                    margin={{
                                    top: 20, right: 30, left: 20, bottom: 5,
                                    }}
                                >
                                    <CartesianGrid strokeDasharray="3 3" />
                                    <XAxis dataKey="name" />
                                    <YAxis />
                                    <Tooltip />
                                    <Legend />
                                    <Bar dataKey="da" stackId="a" fill="#8884d8" />
                                    <Bar dataKey="ne" stackId="a" fill="#82ca9d" />
                                </BarChart>
							</CardBody>
						</Card>
					</GridItem>
                    <GridItem xs={12} sm={12} md={12}>
						<Card>
							<CardHeader color="success">
								<h4 className={classes.cardTitleWhite}>
                                    Alk Phosphate, Bilirubin, Protime, Sgot
								</h4>
							</CardHeader>
							<CardBody>
                                <p>Alk Phosphate</p>
                                <ComposedChart
                                    width={500}
                                    height={400}
                                    data={alk_phosphateData}
                                    margin={{
                                    top: 20, right: 20, bottom: 20, left: 20,
                                    }}
                                >
                                    <CartesianGrid stroke="#f5f5f5" />
                                    <XAxis dataKey="name" />
                                    <YAxis />
                                    <Tooltip />
                                    <Legend />
                                    <Bar dataKey="total" barSize={20} fill="#413ea0" />
                                    <Line type="monotone" dataKey="total" stroke="#ff7300" />
                                </ComposedChart>
                                <p>Bilirubin</p>
                                <ComposedChart
                                    width={500}
                                    height={400}
                                    data={bilirubinData}
                                    margin={{
                                    top: 20, right: 20, bottom: 20, left: 20,
                                    }}
                                >
                                    <CartesianGrid stroke="#f5f5f5" />
                                    <XAxis dataKey="name" />
                                    <YAxis />
                                    <Tooltip />
                                    <Legend />
                                    <Bar dataKey="total" barSize={20} fill="#413ea0" />
                                    <Line type="monotone" dataKey="total" stroke="#ff7300" />
                                </ComposedChart>
                                <p>Protime</p>
                                <ComposedChart
                                    width={500}
                                    height={400}
                                    data={protimeData}
                                    margin={{
                                    top: 20, right: 20, bottom: 20, left: 20,
                                    }}
                                >
                                    <CartesianGrid stroke="#f5f5f5" />
                                    <XAxis dataKey="name" />
                                    <YAxis />
                                    <Tooltip />
                                    <Legend />
                                    <Bar dataKey="total" barSize={20} fill="#413ea0" />
                                    <Line type="monotone" dataKey="total" stroke="#ff7300" />
                                </ComposedChart>
                                <p>Sgot</p>
                                <ComposedChart
                                    width={500}
                                    height={400}
                                    data={sgotData}
                                    margin={{
                                    top: 20, right: 20, bottom: 20, left: 20,
                                    }}
                                >
                                    <CartesianGrid stroke="#f5f5f5" />
                                    <XAxis dataKey="name" />
                                    <YAxis />
                                    <Tooltip />
                                    <Legend />
                                    <Bar dataKey="total" barSize={20} fill="#413ea0" />
                                    <Line type="monotone" dataKey="total" stroke="#ff7300" />
                                </ComposedChart>
							</CardBody>
						</Card>
					</GridItem>

                    <GridItem xs={12} sm={12} md={12}>
						<Card>
							<CardHeader color="danger">
								<h4 className={classes.cardTitleWhite}>
                                    Analysis
								</h4>
							</CardHeader>
							<CardBody>
                                {items.map(this.createCheckbox).map((item,i) => { return <ol key={i}>{item}</ol>})}
                                <div onChange={event => this.setGender(event)}>
                                    <input type="radio" value="male" defaultChecked name="gender"/> Male
                                    <input type="radio" value="female" name="gender"/> Female
                                    <input type="radio" value="all" name="gender"/> All
                                </div>
                                <input style={{width: "100px"}} min="1" max="80" type="number" placeholder="Min Age" onChange={(e) => this.handleInputChange(e,"min_age")}></input>
                                <input style={{width: "100px"}} min="1" max="80" type="number" placeholder="Max Age" onChange={(e) => this.handleInputChange(e,"max_age")}></input> <br/>
                                <input style={{width: "100px"}} min="0" max="300" type="number" placeholder="Min Alk phosphate" onChange={(e) => this.handleInputChange(e,"min_alk_phosphate")}></input>
                                <input style={{width: "100px"}} min="0" max="300" type="number" placeholder="Max Alk phosphate" onChange={(e) => this.handleInputChange(e,"max_alk_phosphate")}></input> <br/>
                                <input style={{width: "100px"}} min="0" max="10" type="number" placeholder="Max Bilirubin" onChange={(e) => this.handleInputChange(e,"min_bilirubin")}></input> 
                                <input style={{width: "100px"}} min="0" max="10" type="number" placeholder="Min Bilirubin" onChange={(e) => this.handleInputChange(e,"max_bilirubin")}></input> <br/>
                                <input style={{width: "100px"}} min="0" max="100" type="number" placeholder="Min Protime" onChange={(e) => this.handleInputChange(e,"min_protime")}></input>
                                <input style={{width: "100px"}} min="0" max="100" type="number" placeholder="Max Protime" onChange={(e) => this.handleInputChange(e,"max_protime")}></input> <br/>
                                <input style={{width: "100px"}} min="0" max="650" type="number" placeholder="Min Sgot" onChange={(e) => this.handleInputChange(e,"min_sgot")}></input>
                                <input style={{width: "100px"}} min="0" max="650" type="number" placeholder="Max Sgot" onChange={(e) => this.handleInputChange(e,"max_sgot")}></input>
                                <PieChart width={400} height={400}>
                                    <Pie
                                    data={liveDieData}
                                    cx={200}
                                    cy={200}
                                    labelLine={true}
                                    label={renderCustomizedLabel}
                                    outerRadius={80}
                                    fill="#8884d8"
                                    dataKey="total"
                                    >
                                    {
                                        liveDieData.map((entry, index) => <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />)
                                    }
                                    </Pie>
                                    <Tooltip />
                                </PieChart>
                            </CardBody>
						</Card>
					</GridItem>
				</GridContainer>
			</div>
		);
	}
}

Analysis.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Analysis);
