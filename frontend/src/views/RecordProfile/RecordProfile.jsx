import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardAvatar from "components/Card/CardAvatar.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";

import avatar from "assets/img/faces/marc.jpg";
import axios from "axios";

const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  }
};

class RecordProfile extends React.Component {
  
  state = {
    data: {}
  };
  handleChange(e){
    const {data} = this.state;
    this.setState({data: {...data,[e.target.id]: e.target.value}});
  }
  handleClick(){
    console.log(this.state);
    let { data } = this.state;
    axios.post("/api/create", {...data})
    .then((result) => {
      delete data.class;
      data.sex = data.sex == "male" ? 1 : 0;
      let dataArr = Object.keys(data).map(key => data[key]);
      return axios.post("/api/predict/hepatitis",{dataArr});
    })
    .then(result => {
      alert("Dodjeljenja klasa: " + result.data.class);
    })
    .catch((err) => {
      alert('Error!')
    });
  }
  
  render(){
    const { classes } = this.props;
    return (
      <div>
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <Card>
              <CardHeader color="primary">
                <h4 className={classes.cardTitleWhite}>Insert Record</h4>
              </CardHeader>
              <CardBody>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={3}>
                      <CustomInput
                        labelText="Age"
                        id="age"
                        name="age"
                        formControlProps={{
                          fullWidth: true
                        }}
                        inputProps={{
                          type: "number",
                          onChange: (e) => this.handleChange(e)
                        }}
                        
                      />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                      <CustomInput
                        labelText="Albumin"
                        id="albumin"
                        name="albumin"
                        inputProps={{
                          type: "number",
                          onChange: (e) => this.handleChange(e)
                        }}
                        formControlProps={{
                          fullWidth: true
                        }}
                      />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                      <CustomInput
                        labelText="Alk Phosphate"
                        id="alk_phosphate"
                        name="alk_phosphate"
                        inputProps={{
                          type: "number",
                          onChange: (e) => this.handleChange(e)
                        }}
                        formControlProps={{
                          fullWidth: true
                        }}
                      />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                      <CustomInput
                        labelText="Anorexia"
                        id="anorexia"
                        name="anorexia"
                        inputProps={{
                          type: "number",
                          onChange: (e) => this.handleChange(e)
                        }}
                        formControlProps={{
                          fullWidth: true
                        }}
                      />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={3}>
                      <CustomInput
                        labelText="Antivirals"
                        id="antivirals"
                        name="antivirals"
                        inputProps={{
                          type: "number",
                          onChange: (e) => this.handleChange(e)
                        }}
                        formControlProps={{
                          fullWidth: true
                        }}
                      />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                      <CustomInput
                        labelText="Ascites"
                        id="ascites"
                        name="ascites"
                        inputProps={{
                          type: "number",
                          onChange: (e) => this.handleChange(e)
                        }}
                        formControlProps={{
                          fullWidth: true
                        }}
                      />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                      <CustomInput
                        labelText="Bilirubin"
                        id="bilirubin"
                        name="bilirubin"
                        inputProps={{
                          type: "number",
                          onChange: (e) => this.handleChange(e)
                        }}
                        formControlProps={{
                          fullWidth: true
                        }}
                      />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                      <CustomInput
                        labelText="Class"
                        id="class"
                        name="class"
                        inputProps={{
                          type: "text",
                          onChange: (e) => this.handleChange(e)
                        }}
                        formControlProps={{
                          fullWidth: true
                        }}
                      />
                  </GridItem>
                  
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={3}>
                      <CustomInput
                        labelText="Fatigue"
                        id="fatigue"
                        name="fatigue"
                        inputProps={{
                          type: "number",
                          onChange: (e) => this.handleChange(e)
                        }}
                        formControlProps={{
                          fullWidth: true
                        }}
                      />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                      <CustomInput
                        labelText="Histology"
                        id="histology"
                        name="histology"
                        inputProps={{
                          type: "number",
                          onChange: (e) => this.handleChange(e)
                        }}
                        formControlProps={{
                          fullWidth: true
                        }}
                      />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                      <CustomInput
                        labelText="Big liver"
                        id="liver_big"
                        name="liver_big"
                        inputProps={{
                          type: "number",
                          onChange: (e) => this.handleChange(e)
                        }}
                        formControlProps={{
                          fullWidth: true
                        }}
                      />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                      <CustomInput
                        labelText="Firm liver"
                        id="liver_firm"
                        name="liver_firm"
                        inputProps={{
                          type: "number",
                          onChange: (e) => this.handleChange(e)
                        }}
                        formControlProps={{
                          fullWidth: true
                        }}
                      />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={3}>
                      <CustomInput
                        labelText="Malaise"
                        id="malaise"
                        name="malaise"
                        inputProps={{
                          type: "number",
                          onChange: (e) => this.handleChange(e)
                        }}
                        formControlProps={{
                          fullWidth: true
                        }}
                      />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                      <CustomInput
                        labelText="Protime"
                        id="protime"
                        name="protime"
                        inputProps={{
                          type: "number",
                          onChange: (e) => this.handleChange(e)
                        }}
                        formControlProps={{
                          fullWidth: true
                        }}
                      />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                      <CustomInput
                        labelText="Sex"
                        id="sex"
                        name="sex"
                        inputProps={{
                          type: "text",
                          onChange: (e) => this.handleChange(e)
                        }}
                        formControlProps={{
                          fullWidth: true
                        }}
                      />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                      <CustomInput
                        labelText="Sgot"
                        id="sgot"
                        name="sgot"
                        inputProps={{
                          type: "number",
                          onChange: (e) => this.handleChange(e)
                        }}
                        formControlProps={{
                          fullWidth: true
                        }}
                      />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={3}>
                      <CustomInput
                        labelText="Spiders"
                        id="spiders"
                        name="spiders"
                        inputProps={{
                          type: "number",
                          onChange: (e) => this.handleChange(e)
                        }}
                        formControlProps={{
                          fullWidth: true
                        }}
                      />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                      <CustomInput
                        labelText="Palpable Spleen"
                        id="spleen_palpable"
                        name="spleen_palpable"
                        inputProps={{
                          type: "number",
                          onChange: (e) => this.handleChange(e)
                        }}
                        formControlProps={{
                          fullWidth: true
                        }}
                      />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                      <CustomInput
                        labelText="Steroid"
                        id="steroid"
                        name="steroid"
                        inputProps={{
                          type: "number",
                          onChange: (e) => this.handleChange(e)
                        }}
                        formControlProps={{
                          fullWidth: true
                        }}
                      />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={3}>
                      <CustomInput
                        labelText="Varices"
                        id="varices"
                        name="varices"
                        inputProps={{
                          type: "number",
                          onChange: (e) => this.handleChange(e)
                        }}
                        formControlProps={{
                          fullWidth: true
                        }}
                      />
                  </GridItem>
                </GridContainer>
              </CardBody>
              <CardFooter>
                <Button onClick={() => this.handleClick()} color="primary">Save Record</Button>
              </CardFooter>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
    );
  }
}

export default withStyles(styles)(RecordProfile);
