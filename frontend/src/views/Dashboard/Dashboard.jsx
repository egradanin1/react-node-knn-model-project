import React from "react";
import PropTypes from "prop-types";
import withStyles from "@material-ui/core/styles/withStyles";

import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";

import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";

import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";

import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";

import {
	BarChart,
	Bar,
	Cell,
	XAxis,
	YAxis,
	CartesianGrid,
	Tooltip,
	Legend,
	ComposedChart,
	Line,
	Area,
	PieChart,
	Pie
} from "recharts";

import axios from "axios";

class Dashboard extends React.Component {
	state = {
		value: 0,
		chartData: [],
		attribute: "age"
	};

	componentDidMount() {
		this.fetchData();
	}

	fetchData() {
		const { attribute } = this.state;
		console.log(attribute);
		axios
			.get(`/api/getByAttribute/${attribute}`)
			.then(result => {
				this.setState({ chartData: result.data });
			})
			.catch(err => {
				console.log(err);
			});
	}

	handleSelectChange = e => {
		this.setState({ attribute: e.target.value }, function() {
			this.fetchData();
		});
	};
	render() {
		const { classes } = this.props;
		const { chartData, attribute } = this.state;
		const types = {
			age: "Age",
			albumin: "Albumin",
			alk_phosphate: "Alk phosphate",
			anorexia: "Anorexia",
			antivirals: "Antivirals",
			ascites: "Ascites",
			bilirubin: "Bilirubin",
			class: "Class",
			fatigue: "Fatigue",
			histology: "Histology",
			liver_big: "Liver big",
			liver_firm: "Liver firm",
			malaise: "Malaise",
			protime: "Protime",
			sex: "Sex",
			sgot: "Sgot",
			spiders: "Spiders",
			spleen_palpable: "Spleen palpable",
			steroid: "Steroid",
			varices: "Varices"
		};

		const pieChartTypes = {
			class: {
				positive: "live",
				negative: "die"
			},
			sex: {
				positive: "male",
				negative: "female"
			}
		};
		return (
			<div>
				<Select
					value={attribute}
					onChange={e => this.handleSelectChange(e)}
				>
					{Object.keys(types).map(key => {
						return (
							<MenuItem key={key} value={key}>
								{types[key]}
							</MenuItem>
						);
					})}
					/
				</Select>
				<GridContainer>
					<GridItem xs={12} sm={12} md={12}>
						<Card>
							<CardHeader color="warning">
								<h4 className={classes.cardTitleWhite}>
									{types[attribute]} Distribution
								</h4>
							</CardHeader>
							<CardBody>
								{~["class", "sex"].indexOf(attribute) ? (
									<PieChart width={600} height={600}>
										<Pie
											dataKey="total"
											isAnimationActive={true}
											data={chartData.reduce(
												(data, record) => {
													if (
														record[attribute] ==
														pieChartTypes[attribute]
															.positive
													)
														data[0].total += 1;
													else data[1].total += 1;
													return data;
												},
												[
													{
														name:
															pieChartTypes[
																attribute
															]["negative"],
														total: 0
													},
													{
														name:
															pieChartTypes[
																attribute
															]["positive"],
														total: 0
													}
												]
											)}
											cx={200}
											cy={200}
											outerRadius={80}
											fill="#8884d8"
											label
										/>
										<Tooltip />
									</PieChart>
								) : null}
								{~[
									"anorexia",
									"antivirals",
									"ascites",
									"fatigue",
									"histology",
									"liver_big",
									"liver_firm",
									"malaise",
									"spiders",
									"spleen_palpable",
									"steroid",
									"varices"
								].indexOf(attribute) ? (
									<ComposedChart
										layout="vertical"
										width={800}
										height={400}
										data={chartData.reduce(
											(data, record) => {
												if (record[attribute] == 0)
													data[0].total += 1;
												else data[1].total += 1;
												return data;
											},
											[
												{ name: "NE", total: 0 },
												{ name: "DA", total: 0 }
											]
										)}
										margin={{
											top: 20,
											right: 20,
											bottom: 20,
											left: 20
										}}
									>
										<CartesianGrid stroke="#f5f5f5" />
										<XAxis type="number" />
										<YAxis dataKey="name" type="category" />
										<Tooltip />
										<Legend />
										<Bar
											dataKey="total"
											barSize={20}
											fill="#413ea0"
										/>
									</ComposedChart>
								) : null}
								{~[
									"age",
									"albumin",
									"alk_phosphate",
									"bilirubin",
									"protime",
									"sgot"
								].indexOf(attribute) ? (
									<BarChart
										width={800}
										height={400}
										data={chartData}
										margin={{
											top: 5,
											right: 30,
											left: 20,
											bottom: 5
										}}
									>
										<CartesianGrid strokeDasharray="3 3" />
										<XAxis />
										<YAxis />
										<Tooltip />
										<Legend />
										<Bar
											dataKey={attribute}
											fill="#82ca9d"
										/>
									</BarChart>
								) : null}
							</CardBody>
						</Card>
					</GridItem>
				</GridContainer>
			</div>
		);
	}
}

Dashboard.propTypes = {
	classes: PropTypes.object.isRequired
};

export default withStyles(dashboardStyle)(Dashboard);
