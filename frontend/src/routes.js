
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";


import DashboardPage from "views/Dashboard/Dashboard.jsx";
import RecordProfile from "views/RecordProfile/RecordProfile.jsx";
import TableList from "views/TableList/TableList.jsx";
import Analysis from "views/Analysis/Analysis.jsx";

const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/admin"
  },
  {
    path: "/analysis",
    name: "Analysis",
    icon: "whatshot",
    component: Analysis,
    layout: "/admin"
  },
  {
    path: "/record",
    name: "Insert Record",
    icon: Person,
    component: RecordProfile,
    layout: "/admin"
  },
  {
    path: "/data",
    name: "Data",
    icon: "content_paste",
    component: TableList,
    layout: "/admin"
  }
];

export default dashboardRoutes;
